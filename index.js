const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());


const createUser = require('./lambdas/create-user/index.js');
const getUsers = require('./lambdas/get-users/index.js');
const getUser = require('./lambdas/get-user/index.js');
const updateUser = require('./lambdas/update-user/index.js');
const deleteUser = require('./lambdas/delete-user/index.js');


app.post('/users', async (req, res) => {
    
    const response = await createUser.handler(req.body);
    res.status(response.status);
    res.send(response);
});

app.get('/users', async (req, res) => {
    
    const response = await getUsers.handler(req.body);
    
    res.status(response.status);
    res.send(response);
});

app.get('/user/:key', async (req, res) => {

    const response = await getUser.handler(req.params);
    
    res.status(response.status);
    res.send(response);
});

app.put('/user/:key', async (req, res) => {
    const event = {
        key: req.params.key,
        body: req.body
    }
    const response = await updateUser.handler(event);
    
    res.status(response.status);
    res.send(response);
});

app.delete('/user/:key', async (req, res) => {

    const response = await deleteUser.handler(req.params);
    
    res.status(response.status);
    res.send(response);
});

app.listen(3000, () => {
    console.log('Server is running ...');
});