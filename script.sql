DROP DATABASE  IF EXISTS aws_simulate;
CREATE DATABASE aws_simulate;

CREATE TABLE aws_simulate.users (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    login VARCHAR(50) NOT NULL UNIQUE
);

INSERT INTO aws_simulate.users (name, login) VALUE ('admin', 'admin');

SELECT * FROM aws_simulate.users;
