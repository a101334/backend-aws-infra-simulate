const mysql = require('mysql');

handler = (event) => {
    const conn = mysql.createConnection({
        host: 'localhost',
        user: 'developer',
        password: 'developer'
    });
   return new Promise((resolve, reject) => {
        
        sqlSelect = "";

        if(event && event.key && event.key !== '') {
            const key = event.key;

            if(isNaN(key)) {
                sqlSelect = `SELECT * FROM aws_simulate.users WHERE login='${key}'`;
            } else {
                sqlSelect = `SELECT * FROM aws_simulate.users WHERE id='${key}'`;
            }
            
            return conn.query(sqlSelect, (err, result) => {
                if(!err) {
                    resolve({ 
                        status: 200,
                        body: JSON.stringify(result)
                    });
                } else {
                    resolve({
                        status:400,
                        body: JSON.stringify({
                            message: err.sqlMessage
                        })
                    });
                }
            });

        } else {
            resolve({ 
                status: 400,
                body: JSON.stringify({
                    message: 'Missing parameter key'
                })
            });
        }
   });
}

module.exports = {
    handler,
    mysql
}