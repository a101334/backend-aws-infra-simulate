const mocks = require('./mocks/default');
const lambda = require('../index');

describe('Get user', () => {

    test('Should return status 200 when get user with id', async (done) => {
        const conn = {
            query: jest.fn((params, callback) => {
                callback(null, mocks.RESPONSE_200_QUERY);
            })
        }

        jest.spyOn(lambda.mysql, 'createConnection').mockImplementation((params) => {
            return conn;  
        });

        try {
            const result = await lambda.handler(mocks.EVENT_ID);
            expect(result.status).toEqual(mocks.RESPONSE_SUCCESS_200.status);
            expect(JSON.parse(result.body)).toEqual(JSON.parse(mocks.RESPONSE_SUCCESS_200.body));
            done();
        } catch(e) {
            console.log(e);
            done('Expected success response');
        }
    });

    test('Should return status 200 when get user with login', async (done) => {
        const conn = {
            query: jest.fn((params, callback) => {
                callback(null, mocks.RESPONSE_200_QUERY);
            })
        }

        jest.spyOn(lambda.mysql, 'createConnection').mockImplementation((params) => {
            return conn;  
        });

        try {
            const result = await lambda.handler(mocks.EVENT_LOGIN);
            expect(result.status).toEqual(mocks.RESPONSE_SUCCESS_200.status);
            expect(JSON.parse(result.body)).toEqual(JSON.parse(mocks.RESPONSE_SUCCESS_200.body));
            done();
        } catch(e) {
            console.log(e);
            done('Expected success response');
        }
    });

    test('Should return status 400 when can not connect db', async (done) => {
        const conn = {
            query: jest.fn((params, callback) => {
                callback(mocks.RESPONSE_CALLBACK_ERROR,null);
            })
        }

        jest.spyOn(lambda.mysql, 'createConnection').mockImplementation((params) => {
            return conn;  
        });

        try {
            const response = await lambda.handler(mocks.EVENT_LOGIN);
            expect(response.status).toEqual(mocks.RESPONSE_ERROR_400.status);
            done();
        } catch(e) {
            console.log(e);
            done('Expected error response in the try block');
        }
    });

    test('Should return status 400 when missing parameter key', async (done) => {
        const conn = {
            query: jest.fn((params, callback) => {
                callback(mocks.RESPONSE_CALLBACK_ERROR,null);
            })
        }

        jest.spyOn(lambda.mysql, 'createConnection').mockImplementation((params) => {
            return conn;  
        });

        try {
            const response = await lambda.handler();
            expect(response.status).toEqual(mocks.RESPONSE_ERROR_400.status);
            done();
        } catch(e) {
            console.log(e);
            done('Expected error response in the try block');
        }
    });
});