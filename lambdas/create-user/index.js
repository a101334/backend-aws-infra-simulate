const mysql = require('mysql');

const isEmptyObj = (obj) => {
    for(const key in obj) {
        if(obj.hasOwnProperty(key)) {
            return false;
        }
    }

    return true;
}

const isValidParamters = (event) => {

    if(event && !isEmptyObj(event)) {
        const obligatoryParamters = [
            "name",
            "login"
        ];
        for(const key in event) {
            if((!event[key] && event[key] == '') || !obligatoryParamters.includes(key) || !event.hasOwnProperty(key)) {
                return false;
            }
        }
    
        return true;
    } else {
        return false;    
    }
}

handler = (event) => {
    const conn = mysql.createConnection({
        host: 'localhost',
        user: 'developer',
        password: 'developer'
    });
   return new Promise((resolve, reject) => {
        if(event && isValidParamters(event)) {
            const sqlInsert = `INSERT INTO aws_simulate.users (name, login) VALUE ('${event.name}', '${event.login}')`;
            return conn.query(sqlInsert, (err, result) => {
                if(!err) {
                    resolve({ 
                        status: 200
                    });
                } else {
                    resolve({
                        status:400,
                        body: JSON.stringify({
                            message: err.sqlMessage
                        })
                    });
                }
            });
        }else {
            resolve({
                status:400,
                body: JSON.stringify({
                    message: 'Missing parameters'
                })
            });
        }
   });
}

module.exports = {
    handler
}