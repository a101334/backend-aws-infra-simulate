const EVENT_VALID_NO_DUPLICATE = {
    name: 'name',
    login: 'login'
};

const RESPONSE_SUCCESS_200 = {
    status: 200
};

const EVENT_INVALID_NAME = {
    nme: 'name',
    login: 'login'
};

const EVENT_INVALID_LOGIN = {
    name: 'name',
    logn: 'login'
};

const EVENT_INVALID_EMPTY = {
    
};

const RESPONSE_ERROR_400_PARAMETER = {
    status: 400,
    message: 'Missing parameters'
};

const EVENT_VALID_DUPLICATE = {
    name: 'admin',
    login: 'admin'
};

const RESPONSE_ERROR_400_MYSQL_DUPLICATE = {
    status: 400,
    message: "Duplicate entry 'admin' for key 'login'"
};


module.exports = {
    EVENT_VALID_NO_DUPLICATE,
    RESPONSE_SUCCESS_200,
    EVENT_INVALID_NAME,
    RESPONSE_ERROR_400_PARAMETER,
    EVENT_INVALID_LOGIN,
    EVENT_VALID_DUPLICATE,
    RESPONSE_ERROR_400_MYSQL_DUPLICATE,
    EVENT_INVALID_EMPTY
}