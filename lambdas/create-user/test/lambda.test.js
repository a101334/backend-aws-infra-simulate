const lambda = require('../index');
const mysql = require('mysql');
const mocks = require('./mocks/default');

describe('Create User', () => {

    test('Should return status 200 when the user input the correct parameters and not duplicate login', async () => {

        const conn = {
            query: jest.fn((params, callback) => {
                callback(null, {});
            })
        };

        jest.spyOn(mysql, 'createConnection').mockImplementation((params) => {
            return conn;
        });

        const response = await lambda.handler(mocks.EVENT_VALID_NO_DUPLICATE);
        expect(response.status).toEqual(mocks.RESPONSE_SUCCESS_200.status);
    });

    test('Should return status 400 when the user input the incorrect parameters name', async () => {

        const conn = {
            query: jest.fn((params, callback) => {
                callback({}, null);
            })
        };

        jest.spyOn(mysql, 'createConnection').mockImplementation((params) => {
            return conn;
        });

        const response = await lambda.handler(mocks.EVENT_INVALID_NAME);
        const responseBody = JSON.parse(response.body);
        
        expect(response.status).toEqual(mocks.RESPONSE_ERROR_400_PARAMETER.status);
        expect(responseBody.message).toEqual(mocks.RESPONSE_ERROR_400_PARAMETER.message);
    });

    test('Should return status 400 when the user input the incorrect parameters login', async () => {

        const conn = {
            query: jest.fn((params, callback) => {
                callback({}, null);
            })
        };

        jest.spyOn(mysql, 'createConnection').mockImplementation((params) => {
            return conn;
        });

        const response = await lambda.handler(mocks.EVENT_INVALID_LOGIN);
        const responseBody = JSON.parse(response.body);
        
        expect(response.status).toEqual(mocks.RESPONSE_ERROR_400_PARAMETER.status);
        expect(responseBody.message).toEqual(mocks.RESPONSE_ERROR_400_PARAMETER.message);
    });

    test('Should return status 400 when mysql error duplicate', async () => {

        const conn = {
            query: jest.fn((params, callback) => {
                err = {
                    sqlMessage: "Duplicate entry 'admin' for key 'login'"
                }
                callback(err, null);
            })
        };

        jest.spyOn(mysql, 'createConnection').mockImplementation((params) => {
            return conn;
        });

        const response = await lambda.handler(mocks.EVENT_VALID_DUPLICATE);
        const responseBody = JSON.parse(response.body);
        
        expect(response.status).toEqual(mocks.RESPONSE_ERROR_400_MYSQL_DUPLICATE.status);
        expect(responseBody.message).toEqual(mocks.RESPONSE_ERROR_400_MYSQL_DUPLICATE.message);
    });

    test('Should return status 400 when the body is empty', async () => {

        const conn = {
            query: jest.fn((params, callback) => {
                callback({}, null);
            })
        };

        jest.spyOn(mysql, 'createConnection').mockImplementation((params) => {
            return conn;
        });

        const response = await lambda.handler(mocks.EVENT_INVALID_EMPTY);
        console.log(response);
        const responseBody = JSON.parse(response.body);
        
        expect(response.status).toEqual(mocks.RESPONSE_ERROR_400_PARAMETER.status);
        expect(responseBody.message).toEqual(mocks.RESPONSE_ERROR_400_PARAMETER.message);
    });
});
