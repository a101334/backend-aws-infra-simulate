const mysql = require('mysql');

handler = (event) => {
    const conn = mysql.createConnection({
        host: 'localhost',
        user: 'developer',
        password: 'developer'
    });
   return new Promise((resolve, reject) => {
        
        const sqlSelectAll = `SELECT * FROM aws_simulate.users`;
        return conn.query(sqlSelectAll, (err, result) => {
            if(!err) {
                resolve({ 
                    status: 200,
                    body: JSON.stringify(result)
                });
            } else {
                resolve({
                    status:400,
                    body: JSON.stringify({
                        message: err.sqlMessage
                    })
                });
            }
        });
   });
}

module.exports = {
    handler,
    mysql
}