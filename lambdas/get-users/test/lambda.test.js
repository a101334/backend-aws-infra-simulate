const mocks = require('./mocks/default');
const lambda = require('../index');

describe('Get all users', () => {

    test('Should return status 200 when get all users', async (done) => {
        const conn = {
            query: jest.fn((params, callback) => {
                callback(null, mocks.RESPONSE_QUERY_200);
            })
        }

        jest.spyOn(lambda.mysql, 'createConnection').mockImplementation((params) => {
            return conn;  
        });

        try {
            const result = await lambda.handler();
            expect(result.status).toEqual(mocks.RESPONSE_SUCCESS_200.status);
            expect(JSON.parse(result.body)).toEqual(JSON.parse(mocks.RESPONSE_SUCCESS_200.body));
            done();
        } catch(e) {
            console.log(e);
            done('Expected success response');
        }
    });

    test('Should return status 400 when can not connect db', async (done) => {
        const conn = {
            query: jest.fn((params, callback) => {
                callback(mocks.RESPONSE_CALLBACK_ERROR,null);
            })
        }

        jest.spyOn(lambda.mysql, 'createConnection').mockImplementation((params) => {
            return conn;  
        });

        try {
            const response = await lambda.handler();
            expect(response.status).toEqual(mocks.RESPONSE_ERROR_400.status);
            done();
        } catch(e) {
            console.log(e);
            done('Expected error response in the try block');
        }
    });
});