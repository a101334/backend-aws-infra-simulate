RESPONSE_SUCCESS_200 = {
    status: 200,
    body: JSON.stringify([
        {
           name: "admin",
           login: "admin" 
        },
        {
            name: "test1",
            login: "test1" 
        },
        {
            name: "test2",
            login: "test2" 
        }
    ])
};

RESPONSE_CALLBACK_ERROR = {
    err: {
        sqlMessage: "Sql error message"
    }
}

RESPONSE_ERROR_400 = {
    status: 400,
    body: JSON.stringify({
        message: "Sql error message"
    })
}

RESPONSE_QUERY_200 = [
    {
        name: "admin",
        login: "admin" 
    },
    {
        name: "test1",
        login: "test1" 
    },
    {
        name: "test2",
        login: "test2" 
    }
]


module.exports = {
    RESPONSE_SUCCESS_200,
    RESPONSE_CALLBACK_ERROR,
    RESPONSE_ERROR_400,
    RESPONSE_QUERY_200
}