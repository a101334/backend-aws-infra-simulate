EVENT_ID = {
    key: 1
}

EVENT_LOGIN = {
    key: 'admin'
}

RESPONSE_SUCCESS_200 = {
    status: 200
};

RESPONSE_CALLBACK_ERROR = {
    err: {
        sqlMessage: "Sql error message"
    }
}

RESPONSE_ERROR_400 = {
    status: 400,
    body: JSON.stringify({
        message: "Sql error message"
    })
}

RESPONSE_200_QUERY = [];

module.exports = {
    RESPONSE_SUCCESS_200,
    RESPONSE_CALLBACK_ERROR,
    RESPONSE_ERROR_400,
    EVENT_ID,
    EVENT_LOGIN,
    RESPONSE_200_QUERY
}