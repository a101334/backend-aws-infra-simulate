const mysql = require('mysql');

const isValidParameters = (event) => {
    if(event && event.key && event.key !== '' && event.body) {
        return true;
    } else {
        return false;
    }
}
handler = (event) => {
    const conn = mysql.createConnection({
        host: 'localhost',
        user: 'developer',
        password: 'developer'
    });
   return new Promise((resolve, reject) => {
        
        if(isValidParameters(event)) {
            sqlUpdate = "UPDATE aws_simulate.users SET ";
            const key = event.key;
            const body = event.body;
    
            for(const key in body) {
                sqlUpdate += ` ${key}='${body[key]}',`;
            }
            
            sqlUpdate = sqlUpdate.slice(0, -1);
            
            if(isNaN(key)) {
                sqlUpdate += ` WHERE login='${key}'`;
            } else {
                sqlUpdate += ` WHERE id='${key}'`;
            }
            
            return conn.query(sqlUpdate, (err, result) => {
                if(!err) {
                    resolve({ 
                        status: 200
                    });
                } else {
                    resolve({
                        status:400,
                        body: JSON.stringify({
                            message: err.sqlMessage
                        })
                    });
                }
            }); 
        } else {
            resolve({
                status:400,
                body: JSON.stringify({
                    message: 'Missing parameters'
                })
            });
        }
   });
}

module.exports = {
    handler,
    mysql
}