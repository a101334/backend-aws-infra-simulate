
# # backend-aws-infra-simulate API

-  [Installation](#installation)

-  [Endpoints](#endpoints)

-  [Team](#team)

---

## Installation

- All the `code` required to get started is nodejs v10+, angular 8 and mysql

  

### Clone

- Clone this repository to your local machine using `https://a101334@bitbucket.org/a101334/backend-aws-infra-simulate.git`

*PS:* The username and password in database is developer , If the username and password not match with your machine, you need change this values in get-started.sh and all lambdas in the folder lambdas.

### Setup script
> run script get-started in the folder root
```shell
$ chmod 777 get-started.sh
$ ./get-started.sh
```
### Setup manually

> start database

- run this commands in the root folder

```shell
$ mysql -u developer -p
$ source script.sql
```

> install package in the root folder
```shell
$ npm i
```
> install each packages in lambdas folder. Each lambda have own dependencies.

```shell
$ cd lambdas/name_lambda
$ npm i
```
---

### To get started

> installation, clone and setup is did.

  

-  **Step 2**

- run in the root folder

```shell
$ node index.js
```

## Endpoints
- POST
-- http://localhost:3000/users

	```json
		event: {
			name: 'name',
			login: 'login'
		}
	```
- GET
-- http://localhost:3000/users
-- http://localhost:3000/users/:key
ps: key can be id or login

- PUT
-- http://localhost:3000/users/:key
	```json
		event: {
			name: 'name', //optional
			login: 'login' //optional
		}
	```
	ps: key can be id or login

- DELETE
-- http://localhost:3000/users/:key
ps: key can be id or login
---

## Team
> a101334 (Alex da Costa)
---

  

## License

  

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

  

-  **[MIT license](http://opensource.org/licenses/mit-license.php)**
