# started database
mysql -u developer -p < script.sql

# install packages nodejs in the folder root
npm i

# install packages nodejs in the each lambdas
cd lambdas/create-user
npm i
cd ../delete-user
npm i
cd ../get-user
npm i
cd ../get-users
npm i 
cd ../update-user
npm i

# started the API
cd ../../
node index.js 
